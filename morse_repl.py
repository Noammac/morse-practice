#!/usr/bin/env python
"""A REPL that translates text to Morse and plays the resulting sound"""

from json import load
from pydub.playback import play
from pydub.audio_segment import AudioSegment
import morse_to_sound as ms

__author__ = "Noam Barnea"
__license__ = "MIT"

with open("morse_table.json") as f:
    morse_d = {d["symbol"]: d["sequence"] for d in load(f)["table"]}
morse_d[" "] = "/"
dot_time = 0.1
framerate = 44100
frequency = 666


def translate(text: str) -> str:
    return " ".join(morse_d.get(c, "") for c in text.upper())


print("Enter 'quit' or nothing to exit.")
while True:
    rep = input("> ")
    if rep == "" or rep == "quit":
        break
    if rep.isnumeric():
        frequency = int(rep)
        continue
    t = translate(rep)
    data = ms.morse_to_sound(t, frequency=frequency, framerate=framerate, dot_time=dot_time)
    a = AudioSegment(data=data, sample_width=2, frame_rate=framerate, channels=1)
    print(t)
    play(a)
