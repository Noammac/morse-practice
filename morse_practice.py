#!/usr/bin/env python3
"""Practice Morse code, one letter at a time."""
from random import choice, randrange
from json import load, dumps
from pathlib import Path

__author__ = "Noam Barnea"
__license__ = "MIT"

KEY_TEXT = "KEY"
ATTEMPT_TEXT = "ATTEMPTS"
SUCCESS_TEXT = "SUCCEEDED"
PERCENTAGE_TEXT = "PERCENTAGE"
TABLE_PATH = "morse_table.json"
SCORES_PATH = "morse_score.json"

if not Path(TABLE_PATH).is_file():
    raise FileNotFoundError(f"The Morse table file {TABLE_PATH} doesn't exist")

with open(TABLE_PATH, "r") as f:
    table = load(f)["table"]

if Path(SCORES_PATH).is_file():
    with open(SCORES_PATH, "r") as f:
        scores = load(f)
else:
    scores = {}
scores_file = open(SCORES_PATH, "w")

print("Exit the program with Ctrl+C")
try:
    while True:
        index = randrange(len(table))
        key = choice(("sequence", "symbol"))
        oppos = 'sequence' if key == 'symbol' else 'symbol'
        ans = table[index][oppos]
        inp = input(f"Enter {oppos} corresponding to '{table[index][key]}' ").upper()
        if ans not in scores:
            scores.update({ans: [1, 0]})
        else:
            scores.update({ans: [scores[ans][0] + 1, scores[ans][1]]})
        if ans == inp:
            print("Correct!")
            scores.update({ans: [scores[ans][0], scores[ans][1] + 1]})
        else:
            print(f"Incorrect, answer is {ans}")
except KeyboardInterrupt:
    print("Exiting program...")
    print('Writing scores...')
    scores_file.write(dumps(scores, sort_keys=True))
    scores_file.close()
    print()

    pre_tri = max(len(x) for x in scores)  # Using this to reduce expression duplicity, can be fixed with PEP 572
    max_length = pre_tri if pre_tri > len(KEY_TEXT) else len(KEY_TEXT)
    pre_tri = max(len(str(scores[x][0])) for x in scores)  # Same, no sense with not reusing the variable
    max_length_t = pre_tri if pre_tri > len(ATTEMPT_TEXT) else len(ATTEMPT_TEXT)
    pre_tri = max(len(str(scores[x][1])) for x in scores)
    max_length_s = pre_tri if pre_tri > len(SUCCESS_TEXT) else len(SUCCESS_TEXT)

    print(f"{KEY_TEXT.ljust(max_length)} {ATTEMPT_TEXT.ljust(max_length_t)} {SUCCESS_TEXT.ljust(max_length_s)} {PERCENTAGE_TEXT.ljust(len(PERCENTAGE_TEXT))}")
    for key in scores:
        print(f"{key.ljust(max_length)} {str(scores[key][0]).ljust(max_length_t)} {str(scores[key][1]).ljust(max_length_s)} {(scores[key][1]/scores[key][0]):.2%}")
