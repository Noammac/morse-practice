"""A library to translate Morse code into sound"""

import math
import wave
import struct
import sys

__author__ = "Noam Barnea"
__license__ = "MIT"


def sum_morse(morse: str) -> int:
    """Return the amount of time (in dot units) that playing the Morse code version of the given text will take"""
    return sum([time_dict[c]+1 for c in morse])


time_dict = {
            " ": 1,
            "/": 1,
            ".": 1,
            "-": 3
        }
# Between every character within a word there's one dot-unit of space, so we add it no matter what.
# The space between letters in the same word (" ") should be 3 dot-units, it's 1 as it's flanked by two default spaces
# The space between words (" / ") should be 7 dot-units, it's 1 because of default spacing:
# ". / ." one unit of noise, one unit of default space, one unit of character space, one unit of default space,
# one unit of character space, one unit of default space, one unit of character space, one unit of default space,
# 1 + 1 + 1 + 1 + 1 + 1 + 1 = 7


def morse_to_frames(morse, frequency=550, dot_time=0.2, framerate=44100, amplitude=64000):
    """
    Convert Morse code into frames of sound

    :param str morse: The Morse code to convert into sound
    :param int frequency: The frequency of the beeps
    :param int dot_time: The amount of time, in seconds, it would take to play a dot
    :param int framerate: The framerate of the sound, how many frames will play in a second
    :param int amplitude: The amplitude of the sound

    """
    dot_rate = int(framerate * dot_time)  # Rate of frames per dot-length time
    frame_list = []  # Calculate many times, write once!

    for c in morse:
        sine_list = (0,) * (time_dict[c] * dot_rate)  # If it's a silent character
        if c in ["-", "."]:
            sine_list = [math.sin(2 * math.pi * frequency * (x / framerate)) for x in range(time_dict[c] * dot_rate)]
            # ↑ if it isn't
        for n in range(time_dict[c] * dot_rate):  # for every frame that corresponds to this character
            frame_list.append(int(sine_list[n] * amplitude / 2))
        frame_list.extend((0,) * dot_rate)  # Space of 1 dot after each symbol

    return frame_list


def frames_to_bytes(data_size, frames):
    """Convert frames of sound into a byte format"""
    return struct.pack("h" * data_size, *frames)


def morse_to_sound(morse, frequency=550, dot_time=0.2, framerate=44100, amplitude=64000):
    """Convert Morse code to frames of sound in a byte format"""
    return frames_to_bytes(
                           int(framerate * dot_time * sum_morse(morse)),
                           morse_to_frames(morse, frequency=frequency, dot_time=dot_time, framerate=framerate, amplitude=amplitude)
                           )


def morse_to_wav(morse, frequency=550, dot_time=0.2, filename="morse.wav", framerate=44100, amplitude=64000):
    """Convert Morse code into sound and save it in wav format"""
    frame_list = morse_to_frames(morse, frequency, dot_time, framerate, amplitude)
    dot_rate = int(framerate * dot_time)
    data_size = sum_morse(morse) * dot_rate

    wav_file = wave.open(filename, "w")
    wav_file.setparams((1, 2, framerate, data_size, "NONE", "not compressed"))  # number of channels, sample size ... compression type, compression name

    wav_file.writeframes(frames_to_bytes(data_size, frame_list))  # Write once
    wav_file.close()
