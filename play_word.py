"""Choose a random word and play it in Morse code."""

from json import load
from random import choice
from pydub.playback import play
from pydub.audio_segment import AudioSegment
import morse_to_sound as ms

__author__ = "Noam Barnea"
__license__ = "MIT"

with open("morse_table.json") as f:
    morse_table = {d["symbol"]: d["sequence"] for d in load(f)["table"]}
morse_table[" "] = "/"

with open("words_dictionary.json") as f:
    words = [k for k in load(f) if len(k) <= 5]


def tran(text: str) -> str:
    return " ".join(morse_table[c.upper()] for c in text)


word = choice(words)
print(word)
print(tran(word))

audio = AudioSegment(data=ms.morse_to_sound(tran(word)), frame_rate=44100, channels=1, sample_width=2)
play(audio)
