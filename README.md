# Morse Practice Program

## Requirements
All programs require Python 3.6+

Programs that require external libraries require [`jiaaro/pydub`](https://github.com/jiaaro/pydub).

| Program             | Purpose                                                                                                         | External Libraries | `morse_to_sound.py` | `morse_table.json` |
|---------------------|-----------------------------------------------------------------------------------------------------------------|--------------------|---------------------|--------------------|
| `morse_practice.py` | A program to practice letters in Morse code.                                                                    | No                 | No                  | Yes                |
| `morse_to_sound.py` | A library that translates more code to sound.                                                                   | No                 | Yes                 | No                 |
| `morse_repl.py`     | A program that takes words and plays the corresponding Morse code as sound.                                     | Yes                | Yes                 | Yes                |
| `play_word.py`      | A program that chooses a random word and plays it in Morse. Requires a table of words from [`dwyl/english-words`](https://github.com/dwyl/english-words) | Yes                | Yes                 | Yes                |

## Usage
Make sure the program has the required files in its environment (either current directory or in PATH), run with:
```
python name_of_program.py
```
